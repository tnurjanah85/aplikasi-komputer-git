#include<stdio.h>
#include<conio.h>

int main(){
	//kamus luas persegi panjang
	int Pjg, Lbr;
	int Luas_Persegi_Panjang;
	
	//kamus luas persegi
	int Sisi;
	int Luas_Persegi;
	
	//kamus luas segitiga
	int Alas, Tinggi;
	int Luas_Segitiga;

	//algoritma luas persegi panjang
	printf("Masukan Panjang: ");
	scanf("%d", &Pjg);
	printf("Masukan Lebar: ");
	scanf("%d", &Lbr);
	
	//algoritma luas persegi
	printf("Masukan Sisi: ");
	scanf("%d", &Sisi);
	
	//algoritma luas segitiga
	printf("Masukan Alas: ");
	scanf("%d", &Alas);
	printf("Masukan Tinggi: ");
	scanf("%d", &Tinggi);
	
	//rumus luas persegi panjang
	Luas_Persegi_Panjang = Pjg*Lbr;
	
	//rumus luas persegi
	Luas_Persegi = Sisi*Sisi;
	
	//rumus luas segitiga
	Luas_Segitiga = (Alas*Tinggi)/2;
	
	//output hasil luas persegi panjang
	printf("Luas Persegi Panjang = %d\n",Luas_Persegi_Panjang);
	
	//output hasil luas persegi
	printf("Luas Persegi= %d\n",Luas_Persegi);
	
	//output hasil luas segitiga
	printf("Luas Segitiga= %d",Luas_Segitiga);
		return 0;
}
