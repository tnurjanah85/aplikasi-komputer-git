#include<stdio.h>
#include<conio.h>
int main(){
	//kamus luas persegi panjang
	int Pjg, Lbr;
	int Luas_Persegi_Panjang;
	//kamus luas persegi
	int Sisi;
	int Luas_Persegi;
	//kamus luas segitiga
	float Alas, Tinggi;
	float Luas_Segitiga;
	//kamus luas lingkaran
	float phi = 3.14;
	int Jari;
	float Luas_Lingkaran;
	
	//algoritma luas persegi panjang
	printf("Masukan Nilai Untuk Menghitung Luas Persegi Panjang \n");
	printf("Masukan Panjang: ");
	scanf("%d", &Pjg);
	printf("Masukan Lebar: ");
	scanf("%d", &Lbr);
	//algoritma luas persegi
	printf("\nMasukan Nilai Untuk Menghitung Luas Persegi \n");
	printf("Masukan Sisi: ");
	scanf("%d", &Sisi);
	//algoritma luas segitiga
	printf("\nMasukan Nilai Untuk Menghitung Luas Segitiga \n");
	printf("Masukan Alas: ");
	scanf("%f", &Alas);
	printf("Masukan Tinggi: ");
	scanf("%f", &Tinggi);
	//algoritma luas lingkaran
	printf("\nMasukan Nilai Untuk Menghitung Luas Lingkaran \n");
	printf("Masukan Jari-Jari: ");
	scanf("%d", &Jari);
	
	//rumus luas persegi panjang
	Luas_Persegi_Panjang = Pjg*Lbr;
	//rumus luas persegi
	Luas_Persegi = Sisi*Sisi;
	//rumus luas segitiga
	Luas_Segitiga = (Alas*Tinggi)/2;
	//rumus luas lingkaran
	Luas_Lingkaran = phi*Jari*Jari;
	
	//output hasil luas persegi panjang
	printf("\nHasil Nilai Luas \n");
	printf("Luas Persegi Panjang = %d\n",Luas_Persegi_Panjang);
	//output hasil luas persegi
	printf("Luas Persegi= %d\n",Luas_Persegi);
	//output hasil luas segitiga
	printf("Luas Segitiga= %f\n",Luas_Segitiga);
	//output hasil luas lingkaran
	printf("Luas Lingkaran= %f",Luas_Lingkaran);

		return 0;
		
}
